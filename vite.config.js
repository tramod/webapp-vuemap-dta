/* eslint-disable no-undef */
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import { ViteAliases } from 'vite-aliases';
import { dependencies } from './package.json';

const externalPackages = [...Object.keys(dependencies || {})];
const regexesOfPackages = externalPackages.map((packageName) => new RegExp(`^${packageName}(/.*)?`));

/**
 * @type {import('vite').UserConfig}
 */
export default defineConfig(async () => {
  return {
    plugins: [vue(), ViteAliases()],
    optimizeDeps: {
      exclude: ['ol'],
      include: ['pbf', 'mapbox-to-css-font', 'webfont-matcher/lib/fonts/google.js', 'earcut'],
    },
    build: {
      target: 'esnext',
      sourcemap: true,
      minify: false,
      lib: {
        entry: path.resolve(__dirname, 'src/vuemap/index.js'),
        formats: ['es'],
      },
      rollupOptions: {
        external: regexesOfPackages,
        output: {
          preserveModules: true,
          entryFileNames: '[name].[format].js',
        },
      },
    },
    test: {
      globals: true,
      environment: 'jsdom',
    },
  };
});
