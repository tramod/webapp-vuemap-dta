import createVueMapState from './vmState.js';

export const getVueMapMock = () => {
  const vueMap = createVueMapState();

  function mockState({ map, layers } = {}) {
    vueMap._m.set(map.id, map);

    for (const layer of layers) {
      vueMap._l.set(layer.id, layer);
    }
  }

  return { ...vueMap, mockState };
};
