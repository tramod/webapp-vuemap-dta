import { reactive } from 'vue';

export default function createVueMapState() {
  const _m = reactive(new Map());
  const _l = reactive(new Map());
  const _assocs = {};

  function addToMap(map, item) {
    if (!item.isReady) {
      console.warn('Item not ready');
      return;
    }
    if (!_assocs[map.id]) _assocs[map.id] = new Set();
    else if (_assocs[map.id].has(item.id)) {
      console.warn('Item already in map');
      return;
    }

    _assocs[map.id].add(item.id);
    map.olObject.addLayer(item.olObject);
  }

  function removeFromMap(map, item) {
    if (!_assocs[map.id] || !_assocs[map.id].has(item.id)) {
      console.warn('Item not in map');
      return;
    }

    _assocs[map.id].delete(item.id);
    map.olObject.removeLayer(item.olObject);
  }

  function clearMap(map) {
    if (!_assocs[map.id]) _assocs[map.id] = new Set();

    _assocs[map.id].forEach((layerId) => {
      const layerItem = _l.get(layerId);
      removeFromMap(map, layerItem);
    });
  }

  return {
    _m, // Internals, should be used only in vuemap lib
    _l, // Internals, should be used only in vuemap lib
    addToMap,
    removeFromMap,
    clearMap,
  };
}
