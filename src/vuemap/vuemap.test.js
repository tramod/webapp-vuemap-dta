import createVueMap from './vuemap.js';

describe('Plugin', () => {
  it('should have install fn', () => {
    const vm = createVueMap();
    expect(vm.install).to.exist;
    expect(typeof vm.install === 'function').to.true;
  });
});
