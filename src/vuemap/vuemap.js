import { VueMapKey } from './symbols.js';
import createVueMapState from './vmState.js';

// Later should be created individually and not directly by plugin to allow multiple maps
export default function createVueMap() {
  const vm = createVueMapState();
  vm.install = (app) => {
    app.provide(VueMapKey, vm);
  };
  return vm;
}
