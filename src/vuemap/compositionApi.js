import { inject } from 'vue';

import { VueMapKey } from './symbols.js';

export const useVueMap = () => inject(VueMapKey);
