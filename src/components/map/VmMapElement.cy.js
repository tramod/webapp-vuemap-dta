/* eslint-disable vue/one-component-per-file */
import VmMapElement from './VmMapElement.vue';

import { defineComponent, h } from 'vue';

import { defineMap, defineLayer, tileLayer, VmLayer } from '@vuemap/index.js';

/*
  Strictly only first test checks if VmMapElements bound OlMap correctly to DOM
  Other tests checks broader VueMap OlMap integrations and sync
*/
describe('VmMapElement', () => {
  let map;
  const useMainMap = defineMap('mainMap', {
    viewOptions: { zoom: 2, center: [0, 0] },
  });
  const useOSMBaseLayer = defineLayer('OSM', {
    layerType: tileLayer,
    sourceOptions: { type: 'OSM' },
  });

  const PlainMap = defineComponent({
    components: { VmMapElement },
    setup() {
      const _map = useMainMap();
      map = _map;

      return () => h(VmMapElement, { vmMap: _map, class: 'map-element' });
    },
  });

  const MapWithLayer = defineComponent({
    components: { VmMapElement, VmLayer },
    setup() {
      const _map = useMainMap();
      const layer = useOSMBaseLayer();
      map = _map;

      return () =>
        h(VmMapElement, { vmMap: _map, class: 'map-element' }, { default: () => h(VmLayer, { vmLayer: layer }) });
    },
  });

  it('renders, vuemap olMap matches rendered map', () => {
    cy.mount(PlainMap);
    cy.get('.ol-viewport').should('be.visible');
    cy.get('.ol-viewport').should((vp) => {
      expect(vp[0]).to.equal(map.olObject.getViewport());
    });
    cy.get('canvas').should('not.exist'); // No canvas with plain map
  });

  it('can contain layer', () => {
    cy.mount(MapWithLayer).then(() => {
      console.log(map.view.state.center);
    });
    cy.get('.ol-layer').should('exist');
    cy.get('canvas').should('exist');
    // TODO snapshot? cypress-plugin-snapshots
  });

  it('vuemap map can be interacted, updates also state', () => {
    cy.mount(MapWithLayer).then(() => {
      expect(map.view.state.center).to.deep.equal([0, 0]);
    });

    // Maybe bit unneeded playing with map pan *sweat smile*
    // Extra props needed to simulate PointerEvent (cypress fires only event?)
    // IsPrimary + button needed to pass OL dragstart condition
    // For map two moves are needed (down is not used for this) and there must be at least 2 distance between them (down is used for this lol)
    cy.get('canvas')
      .trigger('pointerdown', 200, 200, { force: true, pointerId: 0, isPrimary: true, button: 0 })
      .trigger('pointermove', 202, 200, { force: true, pointerId: 0 })
      .trigger('pointermove', 300, 300, { force: true, pointerId: 0 })
      .trigger('pointerup', { force: true, pointerId: 0 })
      .then(() => {
        expect(map.view.state.center).not.to.deep.equal([0, 0]);
      });
  });
});
