/* eslint-disable vue/one-component-per-file */
import VmLayer from './VmLayer.vue';

import { defineComponent, h, ref } from 'vue';

import { defineMap, defineLayer, tileLayer, VmMapElement } from '@vuemap/index.js';

describe('VmMapElement', () => {
  let map;
  let layer;
  const useMainMap = defineMap('mainMap', {
    viewOptions: { zoom: 2, center: [0, 0] },
  });
  const useOSMBaseLayer = defineLayer('OSM', {
    layerType: tileLayer,
    sourceOptions: { type: 'OSM' },
  });

  it('can render OL layer (vuemap definition)', () => {
    const MapWithLayer = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const _map = useMainMap();
        const _layer = useOSMBaseLayer();
        map = _map;
        layer = _layer;

        return () =>
          h(VmMapElement, { vmMap: _map, class: 'map-element' }, { default: () => h(VmLayer, { vmLayer: _layer }) });
      },
    });

    cy.mount(MapWithLayer);
    cy.get('.ol-layer').should('exist');
    cy.get('canvas').should('exist');
    cy.get('.ol-layer').then(() => {
      expect(map.olObject.getLayers().item(0)).to.equal(layer.olObject); // After render provided layer is same as layer rendered in wrapper map
    });
    // TODO snapshot? cypress-plugin-snapshots
  });

  it('can render also unwrapped vuemap layer', () => {
    const MapWithLayerUnwrapped = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const _map = useMainMap();
        map = _map;

        return () =>
          h(
            VmMapElement,
            { vmMap: _map, class: 'map-element' },
            { default: () => h(VmLayer, { vmLayer: useOSMBaseLayer }) },
          );
      },
    });

    cy.mount(MapWithLayerUnwrapped);
    cy.get('.ol-layer').should('exist');
  });

  it('removes layer from map on VmLayer unmount', () => {
    const MapWithLayerUnmount = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const _map = useMainMap();
        map = _map;
        const isLayerVisible = ref(true);
        window.setTimeout(() => {
          isLayerVisible.value = false;
        }, 500);

        return () =>
          h(
            VmMapElement,
            { vmMap: _map, class: 'map-element' },
            { default: () => (isLayerVisible.value ? h(VmLayer, { vmLayer: useOSMBaseLayer }) : null) },
          );
      },
    });

    cy.mount(MapWithLayerUnmount);
    cy.get('.ol-layer').should('exist');
    cy.get('.ol-layer')
      .should('not.exist')
      .then(() => {
        expect(map.olObject.getLayers().getLength()).to.equal(0);
      });
  });
});
