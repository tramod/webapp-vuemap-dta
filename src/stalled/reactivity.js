import { unref, watchEffect } from 'vue';

export const onOlReady = (container, runFn) => {
  const testFn = () => {
    const { olObject, element } = unref(container);
    if (olObject !== null && element !== null) return true;
  };

  if (testFn()) runFn();
  else {
    const stopEffect = watchEffect(() => {
      if (testFn()) {
        runFn();
        stopEffect();
      }
    });
  }
};
