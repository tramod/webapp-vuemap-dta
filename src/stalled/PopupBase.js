import { nextTick, watch } from 'vue';

import Overlay from 'ol/Overlay.js';

export default popupBase;

function popupBase({ visibleRef, coordinatesRef, elementRef, emit }) {
  let popup;
  let lastCoordinates = coordinatesRef.value;

  const visibleWatch = watch(visibleRef, (isVisible) => {
    if (isVisible && lastCoordinates) setPopup();
    else closePopup();
  });

  const coordsWatch = watch(coordinatesRef, (coords) => {
    lastCoordinates = coords;
    if (visibleRef.value) setPopup(coords);
  });

  async function setPopup(position = lastCoordinates) {
    if (!popup) return;
    await nextTick(); // Let popup content render properly, so OL auto positioning works correctly
    popup.setPosition(position);
    emit('update:visible', true);
  }

  function closePopup() {
    popup.setPosition(undefined);
    emit('update:visible', false);
  }

  function initialize(map, overlayProps = {}) {
    popup = new Overlay({
      element: elementRef.value,
      autoPan: true,
      autoPanAnimation: {
        duration: 250,
      },
      offset: [-50, -12], // Offset for our popup lead, move up and left, overwrites OL default
      positioning: 'bottom-left', // Our default position above feature, overwrites OL default
      ...overlayProps,
    });
    map.addOverlay(popup);

    if (visibleRef.value && lastCoordinates) setPopup();
  }

  function destroy(map) {
    if (popup) map.removeOverlay(popup);
    visibleWatch();
    coordsWatch(); // TODO test if needed
  }

  return { closePopup, initialize, destroy };
}
