// el - ref of map element
// mapObject - OpenLayers map object
// options - useThrottle - throttling of resize observer callback
// options - interval - throttle interval if throttling is used
import { toRefs } from 'vue';
import { useResizeObserver as observer } from '@vueuse/core';

import { onOlReady } from './reactivity.js';

export default {
  install: function (map, options) {
    const { olObject, element: elementRef } = toRefs(map);
    onOlReady(map, () => {
      resizeObserver(elementRef, olObject.value, options);
    });
    return null;
  },
};

async function resizeObserver(elementRef, map, { useThrottle = true, interval = 100 } = {}) {
  let fn;

  const baseFn = () => {
    map.updateSize();
  };

  if (useThrottle) {
    const { useThrottleFn } = await import('@vueuse/core');
    fn = useThrottleFn(baseFn, interval);
  } else fn = baseFn;

  observer(elementRef.value, fn);
}
