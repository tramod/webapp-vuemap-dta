import { unref } from 'vue';

export const toArray = (items) => {
  const _items = unref(items);
  return Array.isArray(_items) ? _items : [_items];
};

export const unWrapLayer = (layerOrUseLayer) =>
  typeof layerOrUseLayer === 'function' ? layerOrUseLayer() : layerOrUseLayer;
export const getLayerId = (layerOrId) => (typeof layerOrId === 'string' ? layerOrId : unWrapLayer(layerOrId).id);

export const getDefinedOptions = (options) => {
  return typeof options === 'object' ? options : options();
};
