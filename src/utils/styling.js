import { watch, unref } from 'vue';

export function addStyleOptions(styleFn, ...options) {
  return function styleFnWithOptions(feature, resolution, fnOptions = {}) {
    for (const option of options) {
      const rawOpt = unref(option);
      Object.assign(fnOptions, typeof rawOpt === 'function' ? rawOpt(feature) : rawOpt);
    }
    return styleFn(feature, resolution, fnOptions);
  };
}

export function styleFilter(styleFn, condition) {
  return function styleFnFilter(feature, ...args) {
    return condition(feature) ? styleFn(feature, ...args) : [];
  };
}

export function watchRender(watchTarget, layers, watchOptions = {}) {
  const _layers = Array.isArray(layers) ? layers : [layers];
  watch(
    watchTarget,
    () => {
      for (const layer of _layers) {
        if (layer.isReady) layer.olObject.changed();
      }
    },
    watchOptions,
  );
}
