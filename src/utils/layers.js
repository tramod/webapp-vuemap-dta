import { toRaw } from 'vue';

export const parseSource = sourceParser;
export const parseVectorLoader = vectorLoaderParser;
export const mergeOptions = mergeOptionsFn;

async function sourceParser(sourceDefinition, sourceGetter) {
  const { sourceType, sourceOptions } = _parseSourceType(sourceDefinition);
  const { default: sourceClass } = await sourceGetter(sourceType);
  return { sourceClass, sourceOptions };
}

function _parseSourceType(sourceDefinition) {
  if (typeof sourceDefinition === 'string')
    return {
      sourceType: sourceDefinition,
      sourceOptions: {},
    };
  else
    return {
      sourceType: sourceDefinition?.type,
      sourceOptions: sourceDefinition,
    };
}

async function vectorLoaderParser(sourceOptions, formatGetter) {
  const sourceLoaderOptions = {};
  if (sourceOptions.features?.length > 0) sourceLoaderOptions.features = toRaw(sourceOptions.features);
  else if (sourceOptions.url || sourceOptions.loader) {
    const { format, formatOptions = {} } = sourceOptions;
    const { default: formatClass } = await formatGetter(format);
    sourceLoaderOptions.format = new formatClass(formatOptions);
    if (sourceOptions.url) sourceLoaderOptions.url = sourceOptions.url;
    else sourceLoaderOptions.loader = sourceOptions.loader;
  } else console.warn('Source of created layer will be empty.');
  return sourceLoaderOptions;
}

// Options merger, expect to have full set in defaults
// Might be useful in other places - subject to change
function mergeOptionsFn(optionsToMerge, defaultOptions) {
  const keys = Object.keys(defaultOptions);
  return keys.reduce((options, key) => {
    options[key] = optionsToMerge[key] ?? defaultOptions[key];
    return options;
  }, {});
}
