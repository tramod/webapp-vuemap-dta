import geojsonvt from 'geojson-vt';
import GeoJSON from 'ol/format/GeoJSON.js';

const worker = self;
const tileIndexes = {};

worker.addEventListener('message', (event) => {
  if (event.data.eventRef.startsWith('TileIndex')) buildTileIndex(event.data, event.data.options);
  if (event.data.eventRef.startsWith('TileLoad')) getTileLoadGeojson(event.data);
});

function buildTileIndex({ data, layerId, eventRef } = {}, { dataProjection, promoteId } = {}) {
  const featureData = dataProjection ? transformSourceData(data, dataProjection) : data;
  tileIndexes[layerId] = geojsonvt(featureData, {
    extent: 4096,
    maxZoom: 20,
    buffer: 128,
    promoteId,
    indexMaxZoom: 14,
  });
  worker.postMessage({ eventRef });
}

function getTileLoadGeojson({ tileCoord, eventRef, layerId } = {}) {
  const vectorTiles = tileIndexes[layerId].getTile(tileCoord[0], tileCoord[1], tileCoord[2]);
  const geojson = {
    type: 'FeatureCollection',
    features: vectorTiles ? vectorTiles.features.map(vtToGeojson) : [],
  };
  // Not necessary to stringify geojson, as geojson doesnt have functions in tree
  worker.postMessage({ geojson, eventRef });
}

function transformSourceData(data, dataProjection) {
  const transformFormat = new GeoJSON();
  const features = transformFormat.readFeatures(data, { dataProjection, featureProjection: 'EPSG:4326' });
  return transformFormat.writeFeaturesObject(features);
}

function vtToGeojson(vt) {
  if (vt.geometry) {
    let type;
    const rawType = vt.type;
    let geometry = vt.geometry;

    if (rawType === 1) {
      type = 'MultiPoint';
      if (geometry.length == 1) {
        type = 'Point';
        geometry = geometry[0];
      }
    } else if (rawType === 2) {
      type = 'MultiLineString';
      if (geometry.length == 1) {
        type = 'LineString';
        geometry = geometry[0];
      }
    } else if (rawType === 3) {
      type = 'Polygon';
      if (geometry.length > 1) {
        type = 'MultiPolygon';
        geometry = [geometry];
      }
    }

    return {
      type: 'Feature',
      geometry: {
        type: type,
        coordinates: geometry,
      },
      properties: vt.tags,
      id: vt.id,
    };
  } else {
    return vt;
  }
}
