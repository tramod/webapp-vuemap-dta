import { watchEffect, computed } from 'vue';

import { toArray } from '@utils/helpers.js';

const isItemReady = (item) => item.isReady === true;

// Unwrap (call defineX) items while in setup
export const unwrapItem = (item) => (typeof item === 'function' ? item() : item);

export const unwrapItems = (items) => toArray(items).map(unwrapItem);

// Accepts only unwrapped items so it can work out of setup run
export const loadVmItems = (items) => {
  toArray(items).forEach((item) => item.load && item.load());
};

export const areVmItemsReady = (items) => {
  return computed(() => toArray(items).every(isItemReady));
};

export const onVmItemsReady = (items, runFn) => {
  let canceled = false;
  const _items = toArray(items);

  const testFn = () => _items.every(isItemReady);
  const cancel = () => (canceled = true);

  if (testFn()) runFn();
  else {
    loadVmItems(_items);
    const stopEffect = watchEffect(() => {
      if (testFn() && !canceled) {
        runFn();
        stopEffect();
      }
    });
  }

  return cancel;
};
