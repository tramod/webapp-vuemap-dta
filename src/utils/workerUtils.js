export class WorkerMessenger {
  constructor(worker) {
    this.list = new Map();
    this.worker = worker;
    this.init();
  }

  init() {
    this.worker.addEventListener('message', (event) => this.callback(event));
  }

  callback(event) {
    const { eventRef } = event.data;
    const resolver = this.list.get(eventRef);
    if (resolver) {
      this.list.delete(eventRef);
      resolver(event.data);
    }
  }

  registerListener(eventRef, resolver, eventData) {
    this.list.set(eventRef, resolver);
    this.worker.postMessage({ eventRef, ...eventData });
  }
}
