import VectorLayer from 'ol/layer/Vector.js';

import { parseSource, parseVectorLoader } from '@utils/layers';

export default async function setupVectorLayer(layer, { layerOptions, sourceOptions } = {}) {
  const { sourceClass, _sOpts } = await parseSource(sourceOptions, getSourceClass);
  const sourceLoaderOptions = await parseVectorLoader(sourceOptions, getFormatClass);

  const source = new sourceClass({
    ..._sOpts,
    ...sourceLoaderOptions,
  });

  const olObject = new VectorLayer({
    zIndex: 15,
    ...layerOptions,
    id: layer.id,
    source,
  });

  return { olObject, source };
}

// TODO should check but this might be possibly only choice for this layer, nonsense to lazy load then
function getSourceClass(key) {
  switch (key) {
    case 'Vector':
      return import('ol/source/Vector.js');
    default:
      throw new Error(`Source ${key} is not available in this layer.`);
  }
}

function getFormatClass(key) {
  switch (key) {
    case 'GeoJSON':
      return import('ol/format/GeoJSON.js');
    case 'GPX':
      return import('ol/format/GPX.js');
    case 'KML':
      return import('ol/format/KML.js');
    case 'TopoJSON':
      return import('ol/format/TopoJSON.js');
    default:
      throw new Error(`Format ${key} is not available in this layer.`);
  }
}
