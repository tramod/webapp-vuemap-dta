import TileLayer from 'ol/layer/Tile.js';

import { parseSource } from '@utils/layers';

function getSourceClass(key) {
  switch (key) {
    case 'OSM':
      return import('ol/source/OSM.js');
    case 'WMTS':
      return import('ol/source/WMTS.js');
    case 'TileJSOM':
      return import('ol/source/TileJSON.js');
    case 'TileWMS':
      return import('ol/source/TileWMS.js');
    default:
      throw new Error(`Source ${key} is not available in this layer.`);
  }
}

export default async function setupTileLayer(layer, { layerOptions, sourceOptions } = {}) {
  const { sourceClass, sourceOptions: _sOpts } = await parseSource(sourceOptions, getSourceClass);
  const source = new sourceClass(_sOpts);

  const olObject = new TileLayer({
    zIndex: 5,
    ...layerOptions,
    id: layer.id,
    source,
  });

  return { olObject, source };
}
