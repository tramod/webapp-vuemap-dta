import { reactive, markRaw, ref } from 'vue';

import { useVueMap } from '@vuemap/index.js';
import { getDefinedOptions } from '@utils/helpers';

export default function defineLayer(layerId, optionsOrOptFunction) {
  async function loadLayer(
    layer,
    { layerType, layerOptions, sourceOptions, plugins = [], typeOptions = {}, preLoadFn, postLoadFn } = {},
  ) {
    if (layer.isReady || layer.isLoading) return;
    layer.isLoading = true;
    if (typeof preLoadFn === 'function') await preLoadFn(layer);

    const { olObject, source } = await layerType(layer, { layerOptions, sourceOptions, ...typeOptions });
    layer.source = markRaw(source);
    layer.olObject = markRaw(olObject);

    for (const plugin of plugins) {
      const _pluginArray = Array.isArray(plugin) ? plugin : [plugin];
      const [{ install: installPlugin, pluginKey }, options] = _pluginArray;
      const pluginObject = installPlugin(layer, options);

      if (pluginKey && pluginObject) {
        if (layer[pluginKey]) console.error(`Cant expose layer plugin ${pluginKey} under key`);
        else layer[pluginKey] = pluginObject;
      }
    }

    if (typeof postLoadFn === 'function') await postLoadFn(layer);
    layer.isLoading = false;
    layer.isReady = true;
  }

  function useMap() {
    const vueMap = useVueMap();
    if (!vueMap._l.has(layerId)) {
      const options = getDefinedOptions(optionsOrOptFunction);
      const layer = getLayerContainer(layerId);
      layer.load = () => loadLayer(layer, options);
      vueMap._l.set(layerId, layer);
      if (!options.lazyLoad) layer.load();
    }

    return vueMap._l.get(layerId);
  }

  return useMap;
}

const getLayerContainer = (id) => {
  const container = reactive({
    id,
    olObject: null,
    source: null,
  });
  container.isLoading = ref(null);
  container.isReady = ref(false);
  return container;
};
