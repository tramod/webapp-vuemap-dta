import { toRef, toRaw, watch, unref } from 'vue';

import { VectorTile } from 'ol/layer.js';
import VectorTileSource from 'ol/source/VectorTile.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import Projection from 'ol/proj/Projection.js';

export default async function setupVectorTileLocalLayer(
  layer,
  { layerOptions, sourceOptions, workerMessenger, extraFeatures = [] } = {},
) {
  layer.tileState = 'empty';
  const data = await getLocalTileData(sourceOptions);
  const source = await createGeojsonVtSource({
    workerMessenger,
    data,
    stateRef: toRef(layer, 'tileState'),
    layerId: layer.id,
    extraFeatures,
    ...sourceOptions,
  });

  const olObject = new VectorTile({
    declutter: true,
    zIndex: 10,
    ...layerOptions,
    id: layer.id,
    source,
  });

  watch(extraFeatures, (newF, oldF) => {
    if (layer.isReady && (oldF.length !== 0 || newF.length !== 0)) layer.source.clear();
  });

  return { olObject, source };
}

async function getLocalTileData(sourceOptions) {
  const { url, data } = sourceOptions;
  if (data) return toRaw(data);
  try {
    const urlResponse = await fetch(url);
    const loadedData = await urlResponse.json();
    return loadedData;
  } catch (error) {
    console.log(error);
    throw new Error('GeojsonVtSource data could not be loaded from url');
  }
}

let tileLoadId = 0;
let tileLoadCounter = 0;

async function createGeojsonVtSource({
  workerMessenger,
  data,
  stateRef,
  layerId,
  extraFeatures,
  dataProjection,
  promoteId,
} = {}) {
  if (!data || !stateRef || !layerId) throw new Error('Mandatory geojsonVtSource param is missing');
  // Request tileIndex build
  stateRef.value = 'build';
  await buildTileIndex(workerMessenger, { data, layerId, options: { promoteId, dataProjection } });
  const format = new GeoJSON({
    // Data returned from geojson-vt is in tile pixel units
    dataProjection: new Projection({
      code: 'TILE_PIXELS',
      units: 'tile-pixels',
      extent: [0, 0, 4096, 4096],
    }),
  });
  const source = new VectorTileSource({
    tileUrlFunction: function (tileCoord) {
      // Use the tile coordinate as a pseudo URL for caching purposes
      return JSON.stringify(tileCoord);
    },
    tileLoadFunction: function (tile) {
      stateRef.value = 'load';
      // TODO add setLoader success callback after OL update (4th param)
      tile.setLoader(function (extent) {
        // Adapted from OL example and from https://github.com/notnotse/ol-geojson-vt
        const tileCoord = tile.getTileCoord();
        // We have to read Geojson here, geojson simple object is easier to send than features
        tileLoadCounter++;
        getTileLoadGeojson(workerMessenger, { tileCoord, layerId }).then(({ geojson }) => {
          const sourceFeatures = format.readFeatures(geojson, {
            extent: extent,
            featureProjection: 'EPSG:3857',
          });
          // Filter extraFeatures which actually intersect tile
          const extraFeaturesInTile = unref(extraFeatures).filter((feature) =>
            feature.getGeometry().intersectsExtent(extent),
          );
          tile.setFeatures([...sourceFeatures, ...extraFeaturesInTile]);
          tileLoadCounter--;
          if (tileLoadCounter === 0) stateRef.value = 'ready';
        });
      });
    },
  });
  stateRef.value = 'ready';
  return source;
}

function buildTileIndex(workerMessenger, options) {
  return new Promise((resolve) => {
    const requestRef = `TileIndex:${options.layerId}`;
    workerMessenger.registerListener(requestRef, resolve, options);
  });
}

function getTileLoadGeojson(workerMessenger, options) {
  // TileLoadId tracked to pair correct geojson to tileLoad (multiple paraller may be running)
  tileLoadId++;
  const requestRef = `TileLoad:${options.layerId}:${tileLoadId}`;
  return new Promise((resolve) => {
    workerMessenger.registerListener(requestRef, resolve, options);
  });
}
