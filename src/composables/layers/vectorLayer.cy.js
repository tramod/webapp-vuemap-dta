/* eslint-disable vue/one-component-per-file */
import vectorLayer from './vectorLayer.js';

import { defineComponent, h } from 'vue';
import { defineMap, defineLayer, VmMapElement, VmLayer } from '@vuemap/index.js';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';

describe('VmMapElement', () => {
  let map;
  let layer;

  it('vector layer added to map from url', () => {
    const useMap = defineMap('map', {
      viewOptions: { zoom: 8, center: [1493000, 6404000] },
    });
    const useUrlVectorLayer = defineLayer('Vector', {
      layerType: vectorLayer,
      sourceOptions: {
        type: 'Vector',
        format: 'GeoJSON',
        url: 'https://gist.githubusercontent.com/ChcJohnie/47de1c9e158650f2d6a0dae8e9cf8db0/raw/88da1cc6eb79419b87fad1986cfb7057144a637c/kraje.geojson',
      },
    });

    const MapWithLayer = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const _map = useMap();
        const _layer = useUrlVectorLayer();
        map = _map;
        layer = _layer;

        return () =>
          h(VmMapElement, { vmMap: _map, class: 'map-element' }, { default: () => h(VmLayer, { vmLayer: _layer }) });
      },
    });

    cy.mount(MapWithLayer);
    cy.get('canvas').should('exist');
    cy.get('.ol-layer').then(() => {
      expect(map.olObject.getLayers().item(0)).to.equal(layer.olObject); // After render provided layer is same as layer rendered in wrapper map
    });
    // TODO snapshot? cypress-plugin-snapshots
  });

  it('vector layer added to map with added features', () => {
    const MapWithLayer = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const useMap = defineMap('map', {
          viewOptions: { zoom: 15, center: [0, 0] },
        });

        const coords = [
          [0, 0],
          [-100, -100],
          [-100, 100],
          [100, -100],
          [100, 100],
        ];
        const features = coords.map((coord) => new Feature({ geometry: new Point(coord) }));
        const useFeaturesVectorLayer = defineLayer('Vector', {
          layerType: vectorLayer,
          sourceOptions: {
            type: 'Vector',
            features,
          },
        });

        const _map = useMap();
        const _layer = useFeaturesVectorLayer();
        map = _map;
        layer = _layer;

        return () =>
          h(VmMapElement, { vmMap: _map, class: 'map-element' }, { default: () => h(VmLayer, { vmLayer: _layer }) });
      },
    });

    cy.mount(MapWithLayer);
    cy.get('canvas').should('exist');
    cy.get('.ol-layer').then(() => {
      expect(map.olObject.getLayers().item(0)).to.equal(layer.olObject); // After render provided layer is same as layer rendered in wrapper map
    });
    // TODO snapshot? cypress-plugin-snapshots
  });
});
