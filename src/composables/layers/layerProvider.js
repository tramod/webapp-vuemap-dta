import { computed, watchEffect } from 'vue';
import { loadVmItems, areVmItemsReady, unwrapItem } from '@utils/reactivity.js';

export default function useLayerProvider(layersToProvide) {
  // ! layers must be already unwrapped/used if pushed out of setup scope
  const isLayerOptions = (lyr) => typeof lyr === 'object' && lyr.layer !== undefined;
  const _layers = computed(() =>
    layersToProvide.map((layerOrOptions) => {
      const isOptions = isLayerOptions(layerOrOptions);
      const layer = unwrapItem(isOptions ? layerOrOptions.layer : layerOrOptions);
      const key = isOptions && layerOrOptions.key !== undefined ? layerOrOptions.key : layer.id;
      return { layer, key };
    }),
  );

  const layersFormatted = computed(() =>
    _layers.value.reduce(
      (out, { layer, key }) => {
        out.hash[key] = layer;
        out.list.push(layer);
        return out;
      },
      { hash: {}, list: [] },
    ),
  );
  const layersList = computed(() => layersFormatted.value.list);

  watchEffect(() => loadVmItems(layersList.value));
  const areLayersLoaded = areVmItemsReady(layersList);
  const layers = computed(() => layersFormatted.value.hash);

  return { layers, areLayersLoaded };
}
