import Select from 'ol/interaction/Select.js';
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import useHover from './Hover.js';
import { shallowRef, ref, reactive, readonly, computed, onUnmounted } from 'vue';

// Simple select binding
// MapContainer - mapContainer, mandatory
/*
  useSelect Options
    initiallyActive[true] - starting active state of select
    initialFeatures[] - features to push into select collection during initialization
    selectedProperty - feature property key to be boolean flag of selection
    onselectFunction - callback function first to run after ol select event (receives event as parameter)
    mode - mode of selection, replacer mode adds option to replace selected feature in collection
    replacer - function to get replacing feature in replacer mode, should return feature with same id as original, receives original as parameter
*/
export default useSelect;
export { useSelectPaging, useSelect };

function useSelect(
  map,
  {
    initiallyActive = true,
    initialFeatures = [],
    selectedProperty,
    onselectFunction,
    olSelectOptions,
    useSelectLayer = true,
    mode = 'standard',
    replacerFunction,
    hovering,
  } = {},
) {
  const selectedFeatures = shallowRef([]);
  const selectedFeaturesIds = computed(() => selectedFeatures.value.map((feature) => feature.getId()));
  const lastSelectEvent = shallowRef({});

  const { initialize, setActive, destroy, isActive, olLayer, selectCollection } = useSelectCore(map, {
    initiallyActive,
    initialFeatures,
    selectOptions: olSelectOptions,
    useSelectLayer,
    hovering,
    setFeaturesFn: setFeatures,
    clearSelectFn: clearSelect,
    onSelectFn: olSelectCallback,
  });

  const setSelectProperty = (feature, state) => feature.set(selectedProperty, state);
  function olSelectCallback(event) {
    if (mode === 'replacer' && typeof replacerFunction === 'function') replaceInSelection(event);
    if (onselectFunction) onselectFunction(event);
    if (selectedProperty) {
      for (const feature of event.deselected) {
        setSelectProperty(feature, false);
      }
      for (const feature of event.selected) {
        setSelectProperty(feature, true);
      }
    }
    selectedFeatures.value = [...event.target.getFeatures().getArray()];
    lastSelectEvent.value = event;
  }

  function setFeatures(features = []) {
    selectCollection.clear();
    selectCollection.extend(features);
    selectedFeatures.value = [...selectCollection.getArray()];
  }

  function clearSelect() {
    selectCollection.clear();
    selectedFeatures.value = [];
  }

  function resetSelect(features = initialFeatures) {
    if (features.length === 0) clearSelect();
    else setFeatures(features);
  }

  // Function to replace original selected feature by feature created by replacer function
  // Also replace selected/deselected by actual feature created by replacer
  function replaceInSelection(event) {
    const selected = event.selected || [];
    const selectedPatch = [];
    const deselectedPatch = [];
    if (selected.length === 0) return;
    for (const tileFeature of selected) {
      const featureId = tileFeature.getId();
      // if feature already selected, it means we are actually deselecting and manual removal is needed
      if (selectedFeaturesIds.value.includes(featureId)) {
        const vectorFeature = selectCollection.getArray().find((f) => f.getId() === featureId);
        selectCollection.remove(vectorFeature);
        deselectedPatch.push(vectorFeature);
        // otherwise get full vector variant of feature and replace it in select collection
      } else {
        const vectorFeature = replacerFunction(tileFeature);
        selectCollection.remove(tileFeature);
        selectCollection.push(vectorFeature);
        selectedPatch.push(vectorFeature);
      }
    }
    event.selected = selectedPatch;
    event.deselected = deselectedPatch;
  }

  initialize();

  return {
    selectedFeatures,
    selectedFeaturesIds,
    lastSelectEvent,
    clearSelect,
    resetSelect,
    setActive,
    destroy,
    isActive,
    ...(useSelectLayer && { olLayer }),
  };
}

function useSelectPaging(map, options) {
  const {
    initialFeatures = [],
    olSelectOptions,
    mode = 'standard',
    replacerFunction = defaultReplacerFn,
    multiLayer,
    useSelectLayer = true,
  } = options;
  const layerRenderWatchHook = useSelectLayer && (options.layerRenderWatchHook ?? true);

  let featLayerAssociations = {};
  const selectedList = shallowRef([]);
  const selectedItem = shallowRef({});
  const listIndex = ref(-1);
  const listLength = computed(() => selectedList.value.length);

  const { initialize, setActive, destroy, isActive, olLayer, select, selectCollection } = useSelectCore(map, {
    ...options,
    initialFeatures,
    selectOptions: { ...olSelectOptions, multi: true, layers: parseOlLayers(), style: parseOlStyle() },
    setFeaturesFn: setFeatures,
    clearSelectFn: clearSelect,
    onSelectFn: (event) => updateSelectList(event.selected),
  });

  const { watchRender, unWatchRender } = useLayerRenderWatchHook(olLayer);

  function updateSelectList(source = []) {
    clearItem();
    featLayerAssociations = {};
    selectedList.value = [...source];
    setItem(0);
  }

  function setItem(index) {
    if (index < 0 || index >= selectedList.value.length || !selectedList.value.length) return;
    clearItem();
    const sourceFeat = selectedList.value[index];
    const featLayer = select.getLayer(sourceFeat);
    const destFeat = mode === 'replacer' ? getReplacerFn(featLayer)(sourceFeat) : sourceFeat;
    featLayerAssociations[destFeat.ol_uid] = featLayer; // ! Ol internals, ol_uid
    selectedItem.value = { feature: destFeat, olLayer: featLayer };
    listIndex.value = index;
    selectCollection.push(destFeat);
    if (layerRenderWatchHook) watchRender(featLayer);
  }

  function clearItem() {
    selectCollection.clear();
    if (!selectedItem.value.feature) return;
    selectedItem.value = {};
    listIndex.value = -1;
    unWatchRender();
  }

  function setFeatures(features = []) {
    updateSelectList(features);
  }

  function clearSelect() {
    updateSelectList();
  }

  function resetSelect(features = initialFeatures) {
    if (features.length === 0) clearSelect();
    else setFeatures(features);
  }

  function parseOlStyle() {
    if (multiLayer) {
      const styleMap = multiLayer.reduce((hash, { layer, style } = {}) => {
        hash[layer.id] = style;
        return hash;
      }, {});
      return function multiLayerSelectStyle(feature, resolution) {
        const featureLayer = featLayerAssociations[feature.ol_uid]; // ! OL internal ol_uid
        const featureStyle = styleMap[featureLayer.get('id')];
        return featureStyle ? featureStyle(feature, resolution) : null;
      };
    } else return olSelectOptions?.style;
  }

  function parseOlLayers() {
    if (!multiLayer) return olSelectOptions?.layers;
    // TODO refactor to work better with vuemap reactivness / improve perf
    const layers = multiLayer.map(({ layer }) => layer);
    return (layer) => layers.map(({ olObject }) => olObject).includes(layer);
  }

  function getReplacerFn(selectedOlLayer) {
    if (multiLayer) {
      const replacerMap = multiLayer.reduce((hash, { layer, replacerFn } = {}) => {
        hash[layer.id] = replacerFn;
        return hash;
      }, {});
      return replacerMap[selectedOlLayer.get('id')] ?? defaultReplacerFn;
    }
    return replacerFunction;
  }

  initialize();

  return {
    paging: reactive({
      selectedList: readonly(selectedList),
      selectedItem: readonly(selectedItem),
      listIndex: readonly(listIndex),
      listLength,
      setItem,
    }),
    clearSelect,
    resetSelect,
    setActive,
    destroy,
    isActive,
    ...(useSelectLayer && { olLayer }),
  };
}

function useSelectCore(
  map,
  {
    initiallyActive = true,
    initialFeatures = [],
    selectOptions = {},
    useSelectLayer = true,
    setFeaturesFn,
    clearSelectFn,
    onSelectFn,
    hovering = false,
    hoverOptions = {},
  } = {},
) {
  let olLayer;
  let hover;
  let destroyed = false;
  const active = ref(false);

  const select = new Select({
    ...selectOptions,
    ...(useSelectLayer && !selectOptions.layers && { layers: (lToSelect) => lToSelect !== olLayer }), // protect select layer if exist no layer filter provided
    style: null,
  });
  const selectCollection = select.getFeatures();

  if (hovering) {
    const layerFilter = selectOptions.olSelectOptions?.layers;
    hover = useHover(map, {
      ...selectOptions,
      layerFilter,
      useHoverLayer: false,
      ...hoverOptions,
    });
  }

  if (useSelectLayer) {
    const selectedSource = new VectorSource({
      features: selectCollection,
    });
    olLayer = new VectorLayer({
      className: 'select-layer',
      style: selectOptions?.style,
      source: selectedSource,
      zIndex: 100,
    });
  }

  select.on('select', onSelectFn);

  function setActive(isActive = true) {
    if (hover) hover.setActive(isActive);
    select.setActive(isActive);
    if (active.value === isActive) return;
    active.value = isActive;
    if (olLayer) map.olObject[isActive ? 'addLayer' : 'removeLayer'](olLayer);
  }

  function initialize() {
    if (initialFeatures) setFeaturesFn(initialFeatures);
    setActive(initiallyActive);
    map.olObject.addInteraction(select);
  }

  function destroy() {
    if (destroyed) return;
    if (active.value) setActive(false);
    if (hover) hover.destroy();
    clearSelectFn();
    map.olObject.removeInteraction(select);
    destroyed = true;
  }

  onUnmounted(destroy);

  return {
    initialize,
    setActive,
    destroy,
    select,
    selectCollection,
    isActive: readonly(active),
    olLayer,
  };
}

function defaultReplacerFn(f) {
  const feature = f.clone();
  feature.setId(f.getId());
  return feature;
}

function useLayerRenderWatchHook(olLayer) {
  let sourceLayer = null;

  const eventName = 'prerender';
  function render() {
    olLayer.getSource().changed();
  }

  function watchRender(_sourceLayer) {
    if (sourceLayer !== _sourceLayer) unWatchRender();
    sourceLayer = _sourceLayer;
    sourceLayer.on(eventName, render);
  }

  function unWatchRender() {
    if (!sourceLayer) return;
    sourceLayer.un('prerender', render);
    sourceLayer = null;
  }

  return { watchRender, unWatchRender };
}
