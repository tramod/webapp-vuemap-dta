import Modify from 'ol/interaction/Modify.js';
import { ref, readonly, onUnmounted } from 'vue';

// Simple OL modify reactive binding
// MapContainer - mapContainer, mandatory
/*
  useModify Options
    initiallyActive[true] - starting active state of interaction
    onModifyStart - modifystart event callback
    onModifyEnd - modifyend event callback
    olModifyOptions - options to pass into modify ol class
*/
export default useModify;

function useModify(map, { initiallyActive = true, onModifyStart, onModifyEnd, olModifyOptions } = {}) {
  const active = ref(false);

  const modify = new Modify({ ...olModifyOptions });
  if (onModifyStart) modify.on('modifystart', onModifyStart);
  if (onModifyEnd) modify.on('modifyend', onModifyEnd);

  function setActive(state = true) {
    modify.setActive(state);
    if (active.value === state) return;
    active.value = state;
  }

  let destroyed = false;
  function destroy() {
    if (destroyed) return;
    if (onModifyStart) modify.un('modifystart', onModifyStart);
    if (onModifyEnd) modify.un('modifyend', onModifyEnd);
    map.olObject.removeInteraction(modify);
    destroyed = true;
  }

  onUnmounted(destroy);

  setActive(initiallyActive);
  map.olObject.addInteraction(modify);

  return {
    modify,
    setActive,
    destroy,
    isActive: readonly(active),
  };
}
