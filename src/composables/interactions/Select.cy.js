/* eslint-disable vue/one-component-per-file */
import { useSelect } from './Select.js';

import { defineComponent, h } from 'vue';
import { defineMap, defineLayer, VmMapElement, VmLayer, vectorLayer } from '@vuemap/index.js';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';

describe('Select', () => {
  let map;
  const F_DATA = [
    { coords: [0, 0], fId: 'test-1' },
    { coords: [100, 100], fId: 'test-2' },
    { coords: [-100, 100], fId: 'test-3' },
    { coords: [100, -100], fId: 'test-4' },
    { coords: [-100, -100], fId: 'test-5' },
  ];

  it('adds selection interaction, sync user action, map state, and vuemap state', () => {
    const MapWithLayer = defineComponent({
      components: { VmMapElement, VmLayer },
      setup() {
        const useMap = defineMap('map', {
          viewOptions: { zoom: 15, center: [0, 0] },
        });

        const features = F_DATA.map(({ coords, fId }) => {
          const f = new Feature({ geometry: new Point(coords) });
          f.setId(fId);
          return f;
        });
        const useFeaturesVectorLayer = defineLayer('Vector', {
          layerType: vectorLayer,
          sourceOptions: {
            type: 'Vector',
            features,
          },
        });

        const _map = useMap();
        const _layer = useFeaturesVectorLayer();
        const { selectedFeaturesIds } = useSelect(_map);
        map = _map;

        return () => [
          h(VmMapElement, { vmMap: _map, class: 'map-element' }, { default: () => h(VmLayer, { vmLayer: _layer }) }),
          h('div', { 'data-test': 'values' }, selectedFeaturesIds.value),
          h('div', { 'data-test': 'length' }, selectedFeaturesIds.value.length),
        ];
      },
    });

    cy.mount(MapWithLayer);
    cy.get('.ol-layer').should('exist');
    cy.get('.ol-viewport').then(($element) => {
      cy.get('[data-test="length"]').should('have.text', 0);
      cy.wrap($element).click(...map.olObject.getPixelFromCoordinate(F_DATA[1].coords));
      cy.get('[data-test="length"]').should('have.text', 1);
      cy.get('[data-test="values"]').should('have.text', F_DATA[1].fId);

      cy.wrap($element).click(
        ...map.olObject.getPixelFromCoordinate(F_DATA[2].coords).map((coord) => Math.round(coord)),
      );
      cy.get('[data-test="values"]').should('have.text', F_DATA[2].fId);

      cy.wrap($element).click(
        ...map.olObject.getPixelFromCoordinate(F_DATA[3].coords).map((coord) => Math.round(coord)),
        {
          shiftKey: true,
        },
      );
      cy.get('[data-test="length"]').should('have.text', 2);
      cy.get('[data-test="values"]').should('have.text', F_DATA[2].fId + F_DATA[3].fId);
    });
  });
});
