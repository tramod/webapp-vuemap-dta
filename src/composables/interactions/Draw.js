import Draw from 'ol/interaction/Draw.js';
import { ref, readonly, onUnmounted } from 'vue';

// Simple OL modify reactive binding
// MapContainer - mapContainer, mandatory
/*
  useModify Options
    initiallyActive[true] - starting active state of interaction
    onModifyStart - modifystart event callback
    onModifyEnd - modifyend event callback
    olModifyOptions - options to pass into modify ol class
*/
export default useDraw;

function useDraw(map, { initiallyActive = true, onDrawStart, onDrawEnd, olDrawOptions } = {}) {
  const active = ref(false);

  const draw = new Draw({ ...olDrawOptions });
  if (onDrawStart) draw.on('drawstart', onDrawStart);
  if (onDrawEnd) draw.on('drawend', onDrawEnd);

  function setActive(state = true) {
    draw.setActive(state);
    if (active.value === state) return;
    active.value = state;
  }

  let destroyed = false;
  function destroy() {
    if (destroyed) return;
    if (onDrawStart) draw.un('drawstart', onDrawStart);
    if (onDrawEnd) draw.un('drawend', onDrawEnd);
    map.olObject.removeInteraction(draw);
    destroyed = true;
  }

  onUnmounted(destroy);

  setActive(initiallyActive); // Needs to be set first, so its not overridden by setter before await finishes
  map.olObject.addInteraction(draw);

  return {
    draw,
    setActive,
    destroy,
    isActive: readonly(active),
  };
}
