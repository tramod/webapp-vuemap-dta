import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
import { ref, readonly, onUnmounted } from 'vue';

// Simple map hover reactive binding
// MapContainer - mapContainer, mandatory
/*
  useHover Options
    initiallyActive[true] - starting active state of hover
    hoveredProperty - feature property key to be boolean flag of hovering
    mode - mode of hover feature, replacer mode adds option to replace hovered feature in hover layer, should be used with replacer function and useHoverLayer option
    replacerFunction - function to get replacing feature in replacer mode, should return feature with same id as original, receives original as parameter
    useHoverLayer - stores hovered feature to internal hover vector layer, with hover-layer classname. Layer is rendered in own canvas so hover change does not force source layer redraw. (Hover source feature is thus kept with original styling underneath hover copy)
    hoverStyle - style to set on internal layer, if not provided, features are automatically styled with their (or theirs layer) original style
    layerFilter - OlMap.forEachFeatureAtPixel optParam (either function or array of allowed layer, if not provided all layers pass)
    hitTolerance - OlMap.forEachFeatureAtPixel optParam (default handling)
*/
export default useHover;

function useHover(
  map,
  {
    initiallyActive = true,
    hoveredProperty = 'isHovered',
    mode = 'standard',
    replacerFunction = defaultReplacerFn,
    useHoverLayer = true,
    hoverStyle,
    layerFilter,
    hitTolerance = 0,
    styleCursor = true,
    styleCursorValue = 'pointer',
  } = {},
) {
  const active = ref(null);

  const _layerFilter =
    typeof layerFilter === 'function'
      ? layerFilter
      : (layerToFilter) => (Array.isArray(layerFilter) ? layerFilter.includes(layerToFilter) : true);

  let olLayer;
  let lastEvent;
  let hoveredFeatures = [];

  if (useHoverLayer) {
    const source = new VectorSource();
    olLayer = new VectorLayer({
      style: hoverStyle,
      source: source,
      zIndex: 101,
      className: 'hover-layer',
    });
  }

  const setHoverProperty = (feature, state) => feature.set(hoveredProperty, state);
  const updateHoveredLayer = (l) => l.getSource().changed();

  async function onHover(event) {
    lastEvent = event;
    if (event.dragging) {
      return;
    }

    if (useHoverLayer) olLayer.getSource().clear();
    else if (hoveredProperty) {
      for (const { feature, layer } of hoveredFeatures) {
        setHoverProperty(feature, false);
        updateHoveredLayer(layer);
      }
      hoveredFeatures = [];
    }

    let featureHit = false;
    const pixel = map.olObject.getEventPixel(event.originalEvent);
    map.olObject.forEachFeatureAtPixel(
      pixel,
      (pixelFeature, pixelLayer) => {
        const feature =
          useHoverLayer && mode === 'replacer' ? replacerFunction(pixelFeature, pixelLayer) : pixelFeature;
        if (!feature) return;
        featureHit = true;
        if (useHoverLayer || hoveredProperty) {
          setHoverProperty(feature, true);
          if (useHoverLayer) {
            olLayer.getSource().addFeature(feature);
            if (!hoverStyle && !feature.getStyle()) feature.setStyle(pixelFeature.getStyle() || pixelLayer.getStyle());
          } else if (hoveredProperty) {
            updateHoveredLayer(pixelLayer);
            hoveredFeatures.push({ feature, layer: pixelLayer });
          }
        }
      },
      {
        layerFilter: _layerFilter,
        hitTolerance,
      },
    );

    if (styleCursor) {
      map.element.style.cursor = featureHit ? styleCursorValue : '';
    }
  }

  const triggerHover = (delay = 0) => {
    if (!lastEvent) return;
    if (!delay) onHover(lastEvent, true);
    else {
      const triggerTimeout = setTimeout(() => {
        onHover(lastEvent, true);
        clearTimeout(triggerTimeout);
      }, 20);
    }
  };

  function setActive(state = true) {
    if (active.value === state) return;
    if (state) map.olObject.on('pointermove', onHover);
    else map.olObject.un('pointermove', onHover);
    active.value = state;
    if (olLayer) {
      if (state) map.olObject.addLayer(olLayer);
      else map.olObject.removeLayer(olLayer);
    }
  }

  function destroy() {
    if (active.value) setActive(false);
  }

  onUnmounted(destroy);
  setActive(active.value ?? initiallyActive);

  return {
    setActive,
    destroy,
    isActive: readonly(active),
    triggerHover,
    olLayer,
  };
}

function defaultReplacerFn(f) {
  const feature = f.clone();
  feature.setId(f.getId());
  return feature;
}
