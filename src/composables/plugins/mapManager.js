import { computed, ref, watch, reactive, readonly } from 'vue';

import { useVueMap } from '@vuemap/index';
import { onVmItemsReady } from '@utils/reactivity';

const pluginKey = 'manager';

export default {
  install(map) {
    const vueMap = useVueMap();
    return mapManager(vueMap, map);
  },
  pluginKey,
};

export { pluginKey };

/*
  [
    code,
    [
      'layer1', // visible, no style
      {
        layer: 'layer2',
        visible: bool = true,
        style: layerStyle //optional
      }
    ]
  ]
*/
function mapManager(vueMap, map) {
  let stateNotFound;
  const currentState = ref('');
  const loadingState = ref(null);
  const statesList = reactive(new Map()); // reactive map works sometimes strangely?
  const statesKeys = computed(() => Array.from(statesList.keys()));
  // These getters were computed, but in some app flows got stuck for no apparent reason?
  const getStateDefinition = () => statesList.get(currentState.value);
  const getStateLayers = () => getStateDefinition()?.layers.map(({ layer }) => layer) ?? [];

  const baseMapList = reactive(new Map());
  const baseMap = ref(null);

  function createMapState(stateKey, stateDef, { setActive = false } = {}) {
    const parsedMapStateDef = _parseMapStateDef(stateDef);
    statesList.set(stateKey, parsedMapStateDef); // If no value, preset key match visible layer
    if (stateNotFound === stateKey || currentState.value === stateKey || setActive) setMapState(stateKey);
  }

  function _parseMapStateDef(stateDef) {
    const parsedLayersDef = stateDef.layers.map((layerOrOptions) => {
      const isOptions = typeof layerOrOptions === 'object' && layerOrOptions.layer !== undefined;
      if (!isOptions) return { layer: unWrapLayer(layerOrOptions) };
      return { ...layerOrOptions, layer: unWrapLayer(layerOrOptions.layer) };
    });
    return { layers: parsedLayersDef };
  }

  function setMapState(stateKey) {
    if (!statesList.has(stateKey)) {
      console.warn(`Preset code ${stateKey} not available`);
      stateNotFound = stateKey;
      return;
    }
    stateNotFound = null;
    if (currentState.value !== stateKey) currentState.value = stateKey;
    _applyMapState(currentState.value);
  }

  function addBaseMap(...layers) {
    for (const layer of layers) {
      const _layer = unWrapLayer(layer);
      baseMapList.set(_layer.id, _layer);
    }
  }

  function setBaseMap(layerOrId = null) {
    baseMap.value = layerOrId ? getLayerId(layerOrId) : null;
  }

  watch(baseMap, (baseMapId, oldBaseMapId) => {
    if (oldBaseMapId) vueMap.removeFromMap(map, baseMapList.get(oldBaseMapId));
    if (baseMapId) _applyBaseMap(baseMapId);
  });

  function _applyBaseMap(baseMapId) {
    const bmLayer = baseMapList.get(baseMapId);
    onVmItemsReady(bmLayer, () => {
      if (baseMap.value !== baseMapId) return;
      vueMap.addToMap(map, bmLayer);
    });
  }

  function _applyMapState(key) {
    if (loadingState.value === key) return;
    const stateDef = getStateDefinition();
    if (!stateDef) return;
    loadingState.value = key;
    vueMap.clearMap(map);
    if (baseMap.value) _applyBaseMap(baseMap.value);

    onVmItemsReady(getStateLayers(), () => {
      if (loadingState.value !== key) return;
      for (const { layer, state, style } of stateDef.layers) {
        vueMap.addToMap(map, layer);
        if (style || state?.style) layer.olObject.setStyle(style || state.style);
      }
      loadingState.value = null;
    });
  }

  function forceRender() {
    for (const layer of getStateLayers()) {
      layer?.olObject?.changed();
    }
  }

  const unWrapLayer = (layerOrUseLayer) =>
    typeof layerOrUseLayer === 'function' ? layerOrUseLayer() : layerOrUseLayer;
  const getLayerId = (layerOrId) => (typeof layerOrId === 'string' ? layerOrId : unWrapLayer(layerOrId).id);

  return {
    createMapState,
    setMapState,
    forceRender,
    addBaseMap,
    setBaseMap,
    state: reactive({
      current: readonly(currentState),
      currentBaseMap: readonly(baseMap),
      loading: computed(() => loadingState.value !== null),
      list: statesKeys,
      layers: computed(() => getStateLayers()), // ! experimental, layers are not properly refresh in some occasions
    }),
  };
}
