import { ref, shallowRef, unref, watch, computed } from 'vue';

export const pluginKey = 'context';

export default {
  install: function (layer, options) {
    return layerContext(layer, options);
  },
  pluginKey,
};
/* 
  Store structure
    {
      contextKey1: {
        featureID1: {}
        featureID2: {}
        ...
      },
      ... 
    }
*/
function layerContext(layer) {
  const store = shallowRef(new Map());
  const context = ref(null);
  const contextData = shallowRef(null);
  const contextKeys = computed(() => Array.from(store.value.keys()));

  const fillContextData = () => (contextData.value = store.value.get(context.value) ?? {});
  fillContextData(); // Fill with defaults, style function may be used before first render can fire

  watch(context, () => render());

  function add(key, dataRef, setAdded = true) {
    if (!['string', 'symbol'].includes(typeof key)) throw new Error(`Invalid context key ${key}`);
    const data = unref(dataRef);
    const isDataObject = typeof data === 'object' && data !== null;
    if (!isDataObject) console.warn('No data or invalid structure added with context');
    const dataObject = isDataObject ? data : {};
    store.value.set(key, dataObject);
    if (setAdded) set(key);
  }

  function set(key) {
    if (!has(key)) console.warn(`Context ${key} not available in store`);
    if (context.value === key) render(); // Force re-render when set with current key
    else context.value = key; // Caught by watch
  }

  function render() {
    if (!context.value) return;
    fillContextData(); // Update contextData manually, computed solution could not be re-evaluated when addContext was called with current context
    layer.source.changed();
  }

  function has(key) {
    return store.value.has(key);
  }

  function getByFeature(fId) {
    const id = typeof fId === 'object' ? fId.getId() : fId;
    return { context: contextData.value?.[id] };
  }

  return {
    add,
    set,
    render,
    has,
    contextData,
    contextKeys,
    getByFeature,
  };
}
