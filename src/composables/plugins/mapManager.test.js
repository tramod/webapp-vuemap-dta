import mapManagerPlugin, { pluginKey } from './mapManager.js';

import { defineComponent } from 'vue';
import { vi } from 'vitest';
import { mount } from '@vue/test-utils';
import { VueMapKey } from '@vuemap/index.js';

const VueMapMock = {
  addToMap: vi.fn(),
  removeFromMap: vi.fn(),
  clearMap: vi.fn(),
};
const mapMock = {};

describe('Map manager (map plugin interface)', () => {
  it('should have install fn and key', () => {
    expect(mapManagerPlugin.install).to.exist;
    expect(typeof mapManagerPlugin.install === 'function').to.true;
    expect(mapManagerPlugin.pluginKey).to.equal(pluginKey);
  });

  it('injects vuemap and setups map manager plugin', () => {
    const WrapperComponent = defineComponent({
      setup() {
        mapManagerPlugin.install(mapMock);
      },
      template: '<span></span>',
    });

    mount(WrapperComponent, {
      global: {
        provide: {
          [VueMapKey]: VueMapMock,
        },
      },
    });

    // No reasonable test ideas for now, at least it runs
    // TODO improve or delete
  });
});
