import { shallowRef, unref, watch } from 'vue';

export const pluginKey = 'features';

export default {
  install: function (layer, options) {
    return featuresReactivity(layer, options);
  },
  pluginKey,
};

function featuresReactivity(layer, { featuresVModel } = {}) {
  const { source } = layer;

  let _oldFeaturesMap;
  let _skipSourceEvent = false;
  const featuresRef = featuresVModel ?? shallowRef(source.getFeatures() || []);

  // Features updates by store, skip rebuild and emit if coming from outside
  // NOTE: not sure if flag reliable
  // NOTE: Feature watching may be non performant on large collection, so optionality should be considered
  source.on('change', () => {
    if (_skipSourceEvent) return;
    const features = source.getFeatures();
    featuresRef.value = features;
    _oldFeaturesMap = _buildFeaturesMap(features);
  });

  // Update features from features array
  watch(featuresRef, updateFeaturesInLayer, { immediate: true });

  function updateFeaturesInLayer(features) {
    const { toAdd, toRemove, featuresMap } = _compareFeatureLists(unref(features), _oldFeaturesMap);
    _oldFeaturesMap = featuresMap;
    _skipSourceEvent = true;
    for (const feature of toRemove) {
      source.removeFeature(feature);
    }
    if (toAdd.length > 0) source.addFeatures(toAdd);
    _skipSourceEvent = false;
  }

  return featuresRef;
}

function _compareFeatureLists(features, oldFeatures = new Map()) {
  const featuresMap = _buildFeaturesMap(features);
  const oldMap = new Map(oldFeatures);

  const toAdd = [];
  for (let [id, feature] of featuresMap) {
    if (oldMap.has(id) && oldMap.get(id) === feature) oldMap.delete(id);
    else toAdd.push(feature);
  }

  return {
    toAdd,
    toRemove: oldMap.values(),
    featuresMap,
  };
}

function _buildFeaturesMap(features) {
  return features.reduce((map, feature) => {
    map.set(feature.getId() || feature.ol_uid, feature);
    return map;
  }, new Map());
}
