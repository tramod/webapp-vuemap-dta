import { reactive, readonly, toRaw, markRaw } from 'vue';

import { View } from 'ol';

export default function createView(viewOptions = {}) {
  // Reactive center did not work, not sure why, should checkout reactivity docs once more
  const local = reactive({
    zoom: null,
    center: null,
    rotation: null,
    ...viewOptions,
  });

  // Exported viewState is not watched so make it clear
  const viewState = readonly(local);

  const view = new View({
    ...local,
  });

  // Build getters and setters
  const propKeys = ['center', 'zoom', 'rotation'];
  const keyCapitalized = (key) => key.charAt(0).toUpperCase() + key.slice(1);
  const getters = propKeys.reduce((map, key) => {
    map[key] = `get${keyCapitalized(key)}`;
    return map;
  }, {});
  const setters = propKeys.reduce((map, key) => {
    map[key] = `set${keyCapitalized(key)}`;
    return map;
  }, {});

  // Observe view changes to view state through map interactions
  for (const key of propKeys) {
    if (key !== 'zoom')
      view.on(`change:${key}`, () => {
        updateIfChanged(key, view[getters[key]]());
      });
  }

  // Zoom can be watched through resolution change
  view.on(`change:resolution`, () => {
    updateIfChanged('zoom', view.getZoom());
  });

  function updateIfChanged(key, value) {
    if (local[key] !== value) {
      local[key] = value;
    }
  }

  function setView(props = {}) {
    for (const [key, value] of Object.entries(props)) {
      if (propKeys.includes(key) && local[key] !== value) {
        const setter = setters[key];
        view[setter](key === 'center' ? toRaw(value) : value);
      }
    }
  }

  return {
    olObject: markRaw(view),
    state: viewState,
    set: setView,
  };
}
