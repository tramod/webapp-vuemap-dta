import { reactive, markRaw, computed, unref } from 'vue';
import { Map } from 'ol';

import { useVueMap } from '@vuemap/index.js';
import { getDefinedOptions } from '@utils/helpers';
import createView from './view.js';

export default function defineMap(mapId, optionsOrOptFunction) {
  function useMap() {
    const vueMap = useVueMap();
    if (!vueMap._m.has(mapId)) {
      const { mapOptions, viewOptions, plugins = [] } = getDefinedOptions(optionsOrOptFunction);
      const map = getMapContainer(mapId);
      const view = createView(viewOptions);
      const olObject = new Map({
        ...mapOptions,
        view: view.olObject,
      });
      map.olObject = markRaw(olObject);
      map.view = view;

      for (const plugin of plugins) {
        const _pluginArray = Array.isArray(plugin) ? plugin : [plugin];
        const [{ install: installPlugin, pluginKey }, options] = _pluginArray;
        const pluginObject = installPlugin(map, options);

        if (pluginKey && pluginObject) {
          if (map[pluginKey]) console.error(`Cant expose map plugin ${pluginKey} under key`);
          else map[pluginKey] = pluginObject;
        }
      }

      vueMap._m.set(mapId, map);
    }

    return vueMap._m.get(mapId);
  }

  return useMap;
}

const getMapContainer = (id) => {
  const container = reactive({
    id,
    element: null,
    olObject: null,
  });
  container.setElement = (domRef) => {
    const element = unref(domRef);
    container.olObject.setTarget(element);
    container.element = element;
  };
  container.isLoading = computed(() => container.element === null);
  container.isReady = computed(() => container.isLoading === false);
  return container;
};
