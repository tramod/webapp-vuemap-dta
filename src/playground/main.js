import { createApp } from 'vue';

import { createVueMap } from '@vuemap/index.js';

import Playground from './App.vue';

const app = createApp(Playground);
app.use(createVueMap());
app.mount('#app');
