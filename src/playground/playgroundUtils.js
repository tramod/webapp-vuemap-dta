import Style from 'ol/style/Style.js';
import Stroke from 'ol/style/Stroke.js';
import Text from 'ol/style/Text.js';
import { Fill, RegularShape } from 'ol/style.js';

const stroke = new Stroke({ color: 'black', width: 2 });
const fill = new Fill({ color: 'red' });

export const crossStyle = new Style({
  image: new RegularShape({
    fill: fill,
    stroke: stroke,
    points: 4,
    radius: 10,
    radius2: 0,
    angle: 0,
  }),
});

export const starStyle = new Style({
  image: new RegularShape({
    fill: fill,
    stroke: stroke,
    points: 5,
    radius: 10,
    radius2: 4,
    angle: 0,
  }),
});

export async function getModelContextData() {
  const urls = [
    'https://bazina.plan4all.eu/caches/tm_pilsen_new/HS-default',
    'https://bazina.plan4all.eu/caches/tm_pilsen_new/HS-CcliR13IVJIM',
    'https://bazina.plan4all.eu/caches/tm_pilsen_new/HS-KxoSEiHP2LLU',
    // 'https://bazina.plan4all.eu/caches/tm_antwerpen/tm-antwerpen-default',
    // 'https://bazina.plan4all.eu/caches/tm_antwerpen/antsin-jS7tUrBhPxMI',
    // 'https://bazina.plan4all.eu/caches/tm_antwerpen/antsin-2q28ktgDEKh8',
  ];

  try {
    const data = await Promise.all(
      urls.map((url) =>
        fetch(url)
          .then((response) => response.json())
          .then((data) => {
            return data.result.traffic.reduce((map, entry) => {
              map[entry.edge_id] = entry.traffic;
              return map;
            }, {});
          }),
      ),
    );

    const dataMap = data.reduce((map, entry, index) => {
      map[index] = entry;
      return map;
    }, {});

    return dataMap;
  } catch (error) {
    console.log(error);

    throw error;
  }
}

// Simplified styling function from TM 1.1
export function modelStyleFn(feature, resolution, { context } = {}) {
  const { volume, intensity } = _baseData(context, feature);
  if (hideFeature(resolution, volume)) return [];
  return trafficStyleCommon({ feature, resolution, volume, intensity });
}

export function currentStyleFn(feature, resolution, { context } = {}) {
  const { volume, intensity } = _baseData(context, feature);
  return trafficStyleCommon({ feature, resolution, volume, intensity });
}

function trafficStyleCommon({ feature, resolution, volume, intensity } = {}) {
  const props = _trafficComp({
    resolution,
    volume,
    intensity,
  });
  return _trafficStyle({
    feature,
    resolution,
    volume,
    ...props,
  });
}

const intensityCoefficient = 1;
const tmColorMode = {
  levels: [0.45, 0.65, 0.85, 1, 100],
  colors: ['#009345', '#F9EC31', '#FAAF40', '#F05A28', '#BE1E2D'],
};

function _baseData(context, feature) {
  const data = {
    volume: context ?? 0,
    capacity: feature.get('capacity'),
  };
  data.intensity = (data.volume / ((data.capacity * 20) / 24)) * intensityCoefficient;
  return data;
}

function _trafficComp({ resolution, volume, intensity }) {
  const volumeLevel = tmColorMode.levels.findIndex((level) => intensity < level);
  const width = _getIntensityWidth(volume, resolution);
  const color = _getIntensityColor(volumeLevel, tmColorMode.colors);
  const text = parseInt(volume).toString();
  const zIndex = volume;
  return {
    width,
    color,
    text,
    zIndex,
  };
}

function _trafficStyle({ resolution, volume = 1, color, width, text = '', zIndex }) {
  if (!volume) return [];
  if (parseInt(volume, 10) === 0) return [];
  const styles = [];
  const usedWidth = volume <= 1 ? _widthResolutionFormula(10, resolution) : width;
  styles.push(
    _adjustBase({
      base: _tsBase,
      width: usedWidth,
      color,
      zIndex,
      text,
    }),
  );
  return styles;
}

const _tsBase = new Style({
  stroke: new Stroke(),
  text: new Text({
    text: '',
    stroke: new Stroke({
      color: '#fff',
      width: 3,
    }),
    placement: 'line',
  }),
});

function _adjustBase({ base, color, width, zIndex } = {}) {
  base.getStroke().setColor(color);
  base.getStroke().setWidth(width);
  // TODO Inspect text setting for VectorTile improvements
  // base.getText().setText(text);
  base.setZIndex(zIndex);
  return base;
}

function _widthResolutionFormula(volume, resolution) {
  return (Math.sqrt(volume) + 1) / Math.sqrt(resolution + 2);
}

function _getIntensityWidth(volume, resolution) {
  const res = _widthResolutionFormula(volume, resolution);
  return res >= 0 ? res : 0;
}

function _getIntensityColor(level, mapping) {
  return mapping[level];
}

function hideFeature(resolution, volume) {
  return volume < resolution * 4 && volume < 2000;
}
