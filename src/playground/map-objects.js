import {
  WorkerMessenger,
  addStyleOptions,
  defineMap,
  defineLayer,
  tileLayer,
  vectorLayer,
  vectorTileLocalLayer,
  layerContextPlugin,
  mapManagerPlugin,
  vectorFeaturesPlugin,
} from '@vuemap/index.js';
import { modelStyleFn, getModelContextData } from './playgroundUtils.js';

export const useMainMap = defineMap('mainMap', {
  viewOptions: { zoom: 10, center: [1493000, 6404000] },
  plugins: [mapManagerPlugin],
});

export const useOSMBaseLayer = defineLayer('OSM', {
  layerType: tileLayer,
  sourceOptions: { type: 'OSM', url: 'https://osm.lesprojekt.cz/osm/{z}/{x}/{y}.png' },
});

export const useCountiesLayer = defineLayer('counties', {
  layerType: vectorLayer,
  lazyLoad: true,
  sourceOptions: {
    type: 'Vector',
    format: 'GeoJSON',
    url: 'https://gist.githubusercontent.com/ChcJohnie/47de1c9e158650f2d6a0dae8e9cf8db0/raw/88da1cc6eb79419b87fad1986cfb7057144a637c/kraje.geojson',
  },
  plugins: [vectorFeaturesPlugin],
});

export const useTrafficLayer = defineLayer('traffic', {
  layerType: vectorTileLocalLayer,
  lazyLoad: true,
  sourceOptions: {
    url: 'https://bazina.plan4all.eu/edges/tm_pilsen_new',
    dataProjection: 'EPSG:3857',
    promoteId: 'edge_id',
  },
  typeOptions: {
    workerMessenger: createWorker(),
  },
  plugins: [layerContextPlugin],
  postLoadFn: async (layer) => {
    const contextData = await getModelContextData();
    const contextDataKeys = Object.keys(contextData);
    for (const key of contextDataKeys) {
      layer.context.add(key, contextData[key], false);
    }
    layer.context.set(contextDataKeys[0]);
    layer.olObject.setStyle(addStyleOptions(modelStyleFn, layer.context.getByFeature));
  },
});

function createWorker() {
  const worker = new Worker(new URL('../utils/workerGeojsonVt.js', import.meta.url), { type: 'module' });

  // In app do
  // const worker = new Worker(new URL('pathToPublic/tileWorker.hash.js', import.meta.url));

  // Log error from worker, will be caught and logged in all layers
  worker.addEventListener(
    'error',
    (event) => {
      console.log(event.message);
    },
    false,
  );

  return new WorkerMessenger(worker);
}
