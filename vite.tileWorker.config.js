/* eslint-disable no-undef */
import path from 'path';

/**
 * @type {import('vite').UserConfig}
 */
export default {
  build: {
    target: 'esnext',
    sourcemap: true,
    minify: 'esbuild',
    lib: {
      entry: path.resolve(__dirname, 'src/utils/workerGeojsonVt.js'),
      fileName: () => 'tileWorker.js',
      name: 'tileWorker',
      formats: ['iife'],
    },
    rollupOptions: {
      output: {
        entryFileNames: () => 'tileWorker.[hash].js',
      },
    },
    emptyOutDir: false,
  },
};
